# Mellow #

A Trello like project mangement system

* Multiple boards
* Collaborate with other users, auto update changes
* Single page app

Built using React, Redux, React-Router and Firebase.

[Live demo](https://mellow-caaa3.firebaseapp.com)