export const BOARD_LIST_CHANGED = 'BOARD_LIST_CHANGED';
export const BOARD_CHANGED = 'BOARD_CHANGED';
export const LIST_CHANGED = 'LIST_CHANGED';
export const CARD_CHANGED = 'CARD_CHANGED';
export const AUTH_STATE_CHANGED = 'AUTH_STATE_CHANGED';
export const ADD_CARD = 'ADD_CARD';
export const ADD_LIST = 'ADD_LIST';
export const ADD_BOARD = 'ADD_BOARD';
export const MOVE_CARD = 'MOVE_CARD';
export const USERNAME_CHANGED = 'USERNAME_CHANGED';
export const ADD_USERNAME = 'ADD_USERNAME';
export const ADD_USER_TO_BOARD = 'ADD_USER_TO_BOARD';

export const boardListChanged = (boardList) => ({
    type: BOARD_LIST_CHANGED,
    payload: boardList
})

export const boardChanged = (board) => ({
    type: BOARD_CHANGED,
    payload: board
})
export const listChanged = (list) => ({
    type: LIST_CHANGED,
    payload: list
})

export const cardChanged = (card) => ({
    type: CARD_CHANGED,
    payload: card
})

export const authStateChanged = (user) => ({
    type: AUTH_STATE_CHANGED,
    payload: user
})

export const usernameChanged = (username) => ({
    type: USERNAME_CHANGED,
    payload: username
})

export const addUsername = (username) => ({
    type: ADD_USERNAME,
    payload: username
})

export const addUserToBoard = (username, boardId) => ({
    type: ADD_USER_TO_BOARD,
    payload: { username, boardId }
})


export const addBoard = (name) => ({
    type: ADD_BOARD,
    payload: { name }
})

export const addList = (name, boardId) => ({
    type: ADD_LIST,
    payload: { name, boardId }
})

export const addCard = (text, listId) => ({
    type: ADD_CARD,
    payload: { text, listId }
})

export const moveCard = (cardId, currentListId, nextListId, position) => ({
    type: MOVE_CARD,
    payload: { cardId, currentListId, nextListId, position }
})

