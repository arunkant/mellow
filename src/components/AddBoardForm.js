import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addBoard } from '../actions';
import uuidv1 from 'uuid';

class AddBoardForm extends Component {
    constructor() {
        super();
        this.state = {
            text: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { text } = this.state;

        this.props.addBoard(text);
        this.setState({ text: "" });
    }
    render() {
        const { text } = this.state;
        return (
            <form onSubmit={this.handleSubmit} autoComplete="off">
                <div className="form-group">
                    <input
                        type="text"
                        className="form-control"
                        id="text"
                        value={text}
                        onChange={this.handleChange}
                        placeholder="New Board..."
                    />
                </div>
                <button type="submit" className="btn btn-success btn-lg" hidden>
                    Add Board
            </button>
            </form>
        );
    }
}


export default connect(
    null, { addBoard }
)(AddBoardForm);
