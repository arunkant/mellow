import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addCard } from '../actions';
import uuidv1 from 'uuid';
import Card from './Card';

class AddCardForm extends Component {
    constructor() {
        super();
        this.state = {
            text: "",
            visiable: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            visiable: !this.state.visiable
        });
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { text } = this.state;
        const { listid } = this.props;
        this.props.addCard(text, listid);
        this.setState({ text: "" });
    }
    render() {
        if(!this.state.visiable) {
            return <button className="btn btn-block" onClick={this.toggle}>Add Card...</button>
        }
        const { text } = this.state;
        return (
            <form onSubmit={this.handleSubmit} autoComplete="off">
                <div className="form-group">
                    <input
                        type="text"
                        className="form-control"
                        id="text"
                        value={text}
                        onChange={this.handleChange}
                        placeholder="New Card..."
                        onBlur={this.toggle}
                    />
                </div>
                <button type="submit" className="btn btn-success btn-lg" hidden>
                    Add card
            </button>
            </form>
        );
    }
}

export default connect(
    null, { addCard }
)(AddCardForm);
