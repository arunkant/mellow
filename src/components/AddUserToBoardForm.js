import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addUserToBoard } from '../actions';

class AddUserToBoard extends Component {
    constructor() {
        super();
        this.state = {
            text: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { text } = this.state;
        const { boardId } = this.props;
        this.props.addUserToBoard(text, boardId);
        this.setState({ text: "" });
    }
    render() {
        const { text } = this.state;
        const { usernameExist } = this.props;
        return (
            <form onSubmit={this.handleSubmit} autoComplete="off">
            {usernameExist && <p> Username already exists please select different username</p>}
                <div className="form-group">
                    <input
                        type="text"
                        className="form-control"
                        id="text"
                        value={text}
                        onChange={this.handleChange}
                        placeholder="username..."
                    />
                </div>
                <button type="submit" className="btn btn-success btn-lg" hidden>Add</button>
            </form>
        );
    }
}

export default connect(
    null, { addUserToBoard }
)(AddUserToBoard);
