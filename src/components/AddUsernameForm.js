import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addUsername } from '../actions';

class AddUsernameForm extends Component {
    constructor() {
        super();
        this.state = {
            text: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { text } = this.state;
        this.props.addUsername(text);
        this.setState({ text: "" });
    }
    render() {
        const { text } = this.state;
        const { usernameExist } = this.props;
        return (
            <form onSubmit={this.handleSubmit} autoComplete="off">
            {usernameExist && <p> Username already exists please select different username</p>}
                <div className="form-group">
                    <input
                        type="text"
                        className="form-control"
                        id="text"
                        value={text}
                        onChange={this.handleChange}
                        placeholder="username..."
                    />
                </div>
                <button type="submit" className="btn btn-success btn-lg">Save</button>
            </form>
        );
    }
}

export default connect(
    state => ({usernameExist: state.usernameExist}), { addUsername }
)(AddUsernameForm);
