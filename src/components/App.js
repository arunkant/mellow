import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { reducer } from "../reducers";
import { stateUpdated } from "../actions";

import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import Board from "./Board";
import TopBar from "./TopBar";
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import AddUsernameForm from './AddUsernameForm';



function mapStateToProps(state) {
    return state;
}

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const currentBoard = null;
        const { uiConfig, firebase, signedIn } = this.props;
        if (!signedIn) {
            return (
                <div>
                    <h1>mellow</h1>
                    <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
                </div>
            );
        }
        if (!this.props.username) {
            return (
                <div>
                    <h3>Please select a username</h3>
                    <AddUsernameForm />
                </div>
            )
        }
        return (
            <Router>
                <div>
                    <TopBar />
                    <Route exact path="/" component={Home} />
                    <Route path="/b/:boardId" component={Board} />
                </div>
            </Router>
        );
    }
}

export default connect(mapStateToProps)(App);

const Home = (props) => (
    <div><p> Please select a board from above</p></div>
)