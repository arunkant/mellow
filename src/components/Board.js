import React, { Component } from 'react';
import List from './List';
import { connect } from 'react-redux';
import AddListForm from "./AddListForm";
import AddUserToBoardForm from './AddUserToBoardForm';

function mapStateToProps(state, ownProps) {
    console.log("board mapper");
    console.dir(ownProps);
    console.dir(state.boards[ownProps.match.params.boardId]);
    return {
        ...state.boards[ownProps.match.params.boardId],
        firebase: state.firebase
    }
}

class Board extends Component {
    constructor(props) {
        super(props);
        this.renderList = this.renderList.bind(this);
    }

    renderList(listid) {
        return (
            <div className="col-sm-3" key={listid.toString()}  >
                <List id={listid} boardId={this.props.id} />
            </div>
        );
    }
    render() {
        const { name, lists, id, firebase, users } = this.props;
        return (
            <div>
                <div className="row">
                    <h1>{name}</h1>
                    <span>users: ({Object.keys(users).join(',')})</span>
                    <div className="col-sm-3 float-right">
                        <AddUserToBoardForm boardId={id} />
                    </div>
                </div>

                <div className="row">
                    {lists && Object.keys(lists).map(this.renderList)}
                    <div className="col-sm-3">
                        <AddListForm boardid={id} firebase={firebase} />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(Board);