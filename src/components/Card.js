import React, { Component } from 'react';
import { connect } from 'react-redux';
import CardAction from './CardAction';

function mapStateToProps(state, ownProps) {
    return state.cards && state.cards[ownProps.id] || {};
}

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewActions: false
        };

        this.toggleAction = this.toggleAction.bind(this);
    }

    toggleAction() {
        this.setState({
            viewActions: !this.state.viewActions
        })
    }
    
    render() {
        const { id, text, listId, boardId, position } = this.props;
        return (
            <div >
                {text}
                <button onClick={this.toggleAction} className={ this.state.viewActions? "btn btn-sm active float-right" : "btn btn-sm float-right" }>...</button>
                {this.state.viewActions && <CardAction listId={listId} boardId={boardId} cardId={id} position={position}/>}
            </div>
        );
    }
}

// export default connect(
//     mapStateToProps,
// )(Card);

// Passing an object full of actions will automatically run each action 
// through the bindActionCreators utility, and turn them into props

export default connect(mapStateToProps)(Card);