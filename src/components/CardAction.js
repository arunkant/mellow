import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { moveCard } from '../actions'

const mapStateToProps = (state, ownProps) => {
    return {
        boards: state.boards,
        lists: state.lists
    }
}

class CardAction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            boardId: props.boardId,
            listId: props.listId,
            position: props.position // initial position
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        this.props.moveCard(this.props.cardId, this.props.listId, this.state.listId, Number(this.state.position));
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    renderBoardOptions() {
        return Object.keys(this.props.boards).map((boardId) => {
            return <option value={boardId} key={boardId}>{this.props.boards[boardId].name}</option>
        });
    }

    renderListOptions() {
        const lists = this.props.boards[this.state.boardId].lists;
        return Object.keys(lists || {}).map((listId) => {
            return <option value={listId} key={listId}>{this.props.lists[listId] && this.props.lists[listId].name || ''}</option>
        });

    }

    renderPositionOptions() {
        const currentListId = this.props.listId;
        const list = this.props.lists[this.state.listId];
        var numOfPositions = Object.keys(list.cards || {}).length; // in case of empty list
        if (currentListId != list.id) {
            numOfPositions += 1;
        }
        return _.range(numOfPositions).map((position) => (
            <option value={position} key={position}>{position}</option>
        ));

    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <small className="text-muted">move card to...</small>
                <div className="form-group">
                    <div className="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Board</span>
                        </div>
                        <select className="custom-select" value={this.state.boardId} onChange={this.onChange} name="boardId">
                            {this.renderBoardOptions()}
                        </select>
                    </div>

                    <div className="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">List</span>
                        </div>
                        <select className="custom-select" value={this.state.listId} onChange={this.onChange} name="listId">
                            {this.renderListOptions()}
                        </select>
                    </div>
                    <div className="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Position</span>
                        </div>
                        <select className="custom-select" value={this.state.position} onChange={this.onChange} name="position">
                            {this.renderPositionOptions()}
                        </select>
                    </div>


                    <button type="submit" className="btn btn-success btn-md float-right"> Move </button>
                </div>
            </form>
        );
    }
}

export default connect(mapStateToProps, { moveCard })(CardAction);