import React, { Component } from 'react';
import Card from './Card';
import { connect } from 'react-redux';
import AddCardForm from "./AddCardForm";
import CSSTransitionGroup from 'react-transition-group';



function mapStateToProps(state, ownProps) {
    return state.lists && state.lists[ownProps.id] || {};
}

class List extends Component {
    constructor(props) {
        super(props);
        this.renderCard = this.renderCard.bind(this);
    }

    renderCard(cardid, position) {
        return (
            <li className="list-group-item p rounded" key={cardid}>
                <Card id={cardid} listId={this.props.id} boardId={this.props.boardId} position={position} />
            </li>
        );
    }
    render() {
        const { name, id, cards } = this.props;
        const renderedCards = cards && Object.keys(cards).sort((key1, key2) => (cards[key1] - cards[key2])).map(this.renderCard) || null;
        return (
            <div className="list p-1 rounded">
                <h5 className="p-1">{name}</h5>
                <ul className="list-group">
                        {renderedCards}
                </ul>
                <AddCardForm listid={id} />
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(List);