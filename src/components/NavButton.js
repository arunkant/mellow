import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeBoard } from '../actions';

class NavButton extends Component {
    constructor() {
        super()
        this.handleOnclick = this.handleOnclick.bind(this);
    }
    handleOnclick() {
        console.log("hello click");
        const { boardid } = this.props;
        this.props.changeBoard(boardid);
    }
    render() {
        const { boardid } = this.props;
        return <button onClick={this.handleOnclick}>{boardid}</button>;
    }
}

export default connect(
    null,
    { changeBoard }
)(NavButton);