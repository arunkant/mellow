import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddBoardForm from './AddBoardForm';
import NavButton from './NavButton';
import { Link } from 'react-router-dom';

function mapStateToProps(state, ownProps) {
    return {
        boardlist: state && state.boardlist || {},
        boards: state && state.boards || {},
        firebase: state && state.firebase || {},
        username: state.username
    }
}

class TopBar extends Component {
    renderBoard(boardid) {
        const board = this.props.boards[boardid];
        console.log("render Board: ")
        console.dir(board);
        const boardName = board && board.name;
        return (
            <li className="list-inline-item nav-item" key={boardid}>
                <Link to={`/b/${boardid}`}><button className="btn btn-secondary">{boardName}</button></Link>
            </li >
        );
    }
    render() {
        const { boards, boardlist, firebase, username } = this.props;
        return (
            <div className="nav">
                <ul className="list-inline p-1">
                    <li className="list-inline-item nav-item">
                        <h2 className="logo">mellow</h2>
                    </li>
                    {Object.keys(boardlist).map(this.renderBoard.bind(this))}
                    <li className="list-inline-item nav-item">
                        <AddBoardForm firebase={firebase} />
                    </li>

                    <li className="list-inline-item nav-item">
                        <p>{firebase.auth().currentUser.displayName}({username}) </p>
                    </li>
                    <li className="list-inline-item nav-item">
                        <a onClick={() => firebase.auth().signOut()}><button className="btn btn-sm">Sign-out</button></a>
                    </li>
                </ul>

            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(TopBar);