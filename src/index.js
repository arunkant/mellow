import React from 'react';
import { render } from 'react-dom';
import { initialState } from "./initialState";
import { boardListChanged, boardChanged, listChanged, cardChanged, authStateChanged, usernameChanged } from "./actions";
import firebase from 'firebase';
import { reducer } from './reducers';
import { Provider } from 'react-redux';

import css from './style.css';

import { createStore } from 'redux';


import App from "./components/App";



const config = {
    apiKey: "AIzaSyBwIIskZ_krfaIwGHQRJWWfqVWh3t8tJzw",
    authDomain: "mellow-caaa3.firebaseapp.com",
    databaseURL: "https://mellow-caaa3.firebaseio.com",
    projectId: "mellow-caaa3",
    storageBucket: "mellow-caaa3.appspot.com",
    messagingSenderId: "1061634349877"
};

firebase.initializeApp(config);

const db = firebase.database();

const store = createStore(reducer, initialState(firebase));
const dispatch = store.dispatch;

firebase.auth().onAuthStateChanged((user) => {
    dispatch(authStateChanged(user));
    db.ref(`${user.uid}/username`).on('value', (usernameSnapshot) => {
        const username = usernameSnapshot.val();
        dispatch(usernameChanged(username));
        db.ref(`boardlists/${username}`).on('value', (boardListSnapshot) => {
            const boardList = boardListSnapshot.val();
            dispatch(boardListChanged(boardList));
            for (const boardId in boardList) {
                var ref = db.ref(`boards/${boardId}`);
                ref.off();
                ref.on('value', (boardSnapshot) => {
                    const board = boardSnapshot.val();
                    dispatch(boardChanged(board));
                    var lists = board.lists;
                    for (const listId in lists) {
                        var ref = db.ref(`lists/${listId}`);
                        ref.off();
                        ref.on('value', (listSnapshot) => {
                            const list = listSnapshot.val();
                            dispatch(listChanged(list));
                            var cards = list.cards;
                            for (const cardId in cards) {
                                var ref = db.ref(`cards/${cardId}`);
                                ref.off();
                                ref.on('value', (cardSnapshot) => {
                                    dispatch(cardChanged(cardSnapshot.val()));
                                })
                            }
                        })
                    }
                })
            }
        })
    })
})

store.subscribe(() => {
    console.log('state');
    console.dir(store.getState());
})

const uiConfig = {
    signInFlow: 'popup',
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID
    ],
    callbacks: {
        // Avoid redirects after sign-in.
        signInSuccess: () => false
    }
};

render(
    <Provider store={store}>
        <App uiConfig={uiConfig} />
    </Provider>,
    document.getElementById('root')
);

window.store = store;