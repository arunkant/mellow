
const prePopulatedState = {
    cards: {
        0: {
            id: 0,
            text: "Hello world"
        }
    },
    lists: {
        1: {
            id: 1,
            name: "First List",
            cards: [0]
        }
    },
    boards: {
        2: {
            id: 2,
            name: "Board 1",
            lists: [1]
        }
    },
    user: {
        boardlist: [2]
    },
    currentBoard: 2
};

const emptyState = {
    cards: {},
    lists: {},
    boards: {0:{
        id: 0,
        name: "Personal Board",
        lists: []
    }},
    signedIn: false
}
export const initialState = (firebase) => ({
    boards: {},
    lists: {},
    cards: {},
    boardlist: {},
    signedIn: false,
    firebase: firebase
})