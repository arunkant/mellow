import { BOARD_LIST_CHANGED, BOARD_CHANGED, LIST_CHANGED, CARD_CHANGED, AUTH_STATE_CHANGED, ADD_BOARD, ADD_LIST, ADD_CARD, MOVE_CARD, USERNAME_CHANGED, ADD_USERNAME, ADD_USER_TO_BOARD } from "../actions";
import _ from 'lodash';

export const reducer = (state, action) => {
    console.log('reducer called with: ');
    console.dir(action);
    console.dir(state);
    switch (action.type) {
        case BOARD_LIST_CHANGED:
            {
                return {
                    ...state,
                    boardlist: action.payload || {}
                };
            }
        case BOARD_CHANGED: {
            return {
                ...state,
                boards: {
                    ...state.boards,
                    [action.payload.id]: action.payload
                }
            };
        }
        case LIST_CHANGED: {
            return {
                ...state,
                lists: {
                    ...state.lists,
                    [action.payload.id]: action.payload
                }
            };
        }
        case CARD_CHANGED: {
            return {
                ...state,
                cards: {
                    ...state.cards,
                    [action.payload.id]: action.payload
                }
            };
        }
        case AUTH_STATE_CHANGED: {
            return {
                ...state,
                signedIn: !!action.payload
            }
        }

        case USERNAME_CHANGED: {
            return {
                ...state,
                username: action.payload
            }
        }
        case ADD_BOARD: {
            setTimeout(() => {
                const db = state.firebase.database();
                const uid = state.firebase.auth().currentUser.uid;
                const username = state.username;
                var keyRef = db.ref(`boards`).push();
                keyRef.set({
                    id: keyRef.key,
                    name: action.payload.name,
                    users: {
                        [username]: true
                    }
                })
                db.ref(`boardlists/${state.username}/${keyRef.key}`).set(true);
            }
                , 0);
            return state;
        }
        case ADD_LIST: {
            // Using set timeout to avoid redux error of dispatching in reducer
            // may use redux-thunk for async if it become bloted
            setTimeout(() => {
                const { name, boardId } = action.payload;
                const db = state.firebase.database();
                var keyRef = db.ref(`lists`).push();
                keyRef.set({
                    id: keyRef.key,
                    name: name,
                });
                console.log("setting list in board: " + `boards/${boardId}/lists/${keyRef.key}`)
                db.ref(`boards/${boardId}/lists/${keyRef.key}`).set(true);
            }
                , 0);
            return state;
        }
        case ADD_CARD: {
            setTimeout(() => {
                const db = state.firebase.database();
                var keyRef = db.ref(`cards`).push();
                const { text, listId } = action.payload
                const nextRank = calculateNextRank(state, listId);
                keyRef.set({
                    id: keyRef.key,
                    text: text,  
                });
                db.ref(`lists/${listId}/cards/${keyRef.key}`).set(nextRank);
            }
                , 0);
            return state;
        }
        case ADD_USERNAME: {
            const db = state.firebase.database();
            const username = action.payload;
            const uid = state.firebase.auth().currentUser.uid;
            setTimeout(() => {
                db.ref(`usernames/${username}`).set(uid);
                db.ref(`${uid}/username`).set(username);
            }
                , 0);
            return state;

        }
        case ADD_USER_TO_BOARD: {
            const { username, boardId } = action.payload;
            const db = state.firebase.database();
            setTimeout(() => {
                db.ref(`boardlists/${username}/${boardId}`).set(true);
                db.ref(`boards/${boardId}/users/${username}`).set(true);
            });
            return state;
        }
        case MOVE_CARD: {
            const { cardId, currentListId, nextListId, position } = action.payload
            let currentPosition = calculateCurrentPosition(state, cardId, currentListId);
            if (currentListId == nextListId && position == currentPosition) return state
            setTimeout(() => {
                let rank = calculateRankForPosition(state, cardId, currentListId, nextListId, position);
                const db = state.firebase.database();
                db.ref(`lists/${currentListId}/cards/${cardId}`).remove();
                db.ref(`lists/${nextListId}/cards/${cardId}`).set(rank);
            })
        }
        default:
            return state;
    }
}

function calculateNextRank(state, listId) {
    const list = state.lists[listId];
    if (!list.cards) return 1;
    const cardIds = Object.keys(list.cards);
    console.log(cardIds);
    if (cardIds.length === 0) return 1;
    const prevMax = _.maxBy(cardIds, (cardId) => list.cards[cardId]);
    return list.cards[prevMax] + 1000000000;
}

function calculateRankForPosition(state, cardId, currentListId, nextListId, position) {
    const cards = state.lists[nextListId].cards;
    const sortedCards = Object.keys(cards || {}).sort((key1, key2) => (cards[key1] - cards[key2]));
    const currentPosition = calculateCurrentPosition(state, cardId, currentListId);
    if (currentListId == nextListId) {
        if (currentPosition == position) {
            return cards[currentPosition]; // Nothing to do here
        } else if (currentPosition > position) {
            if (position == 0) {
                return cards[sortedCards[0]] - 1e9;
            }
            return (cards[sortedCards[position - 1]] + cards[sortedCards[position]]) / 2;
        } else if (currentPosition < position) {
            if (position == sortedCards.length - 1) {
                return cards[sortedCards[sortedCards.length - 1]] + 1e9;
            }
            return (cards[sortedCards[position]] + cards[sortedCards[position + 1]]) / 2;
        }
    } else {
        if (position == 0) {
            return cards ? cards[sortedCards[0]] - 1e9 : 1;
        } else if (position == sortedCards.length) {
            return cards[sortedCards[sortedCards.length - 1]] + 1e9;
        } else {
            return (cards[sortedCards[position - 1]] + cards[sortedCards[position]]) / 2;
        }
    }

}

function calculateCurrentPosition(state, cardId, currentListId) {
    const cards = state.lists[currentListId].cards;
    const sortedCards = Object.keys(cards).sort((key1, key2) => (cards[key1] - cards[key2]));
    return sortedCards.indexOf(cardId);
}