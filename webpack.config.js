const HtmlWebPlugin = require('html-webpack-plugin');
const Path = require('path');

module.exports = {
    devServer: {
        historyApiFallback: true,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    output: {
        publicPath: '/', /* use absolute paths for html-web-plugin "/main.js" */
    },
    plugins: [
        new HtmlWebPlugin({
            template: Path.join(__dirname, 'src', 'index.html')

        }),
    ],
};